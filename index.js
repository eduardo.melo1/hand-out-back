const { app } = require("./app")

const server = app.listen(process.env.PORT || 3333)
console.log(`Listening in ${process.env.PORT || 3333}`)

