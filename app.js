const express = require("express")
require("express-async-errors")
require("dotenv").config()
const cors = require("cors")
const requireDir = require('require-dir')
const app = express()
const path = require("path")
const mongoose = require('mongoose')

//Conecção com o db
mongoose.connect(
    'mongodb+srv://admin:admin@cluster0.wjylb.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
)
requireDir('./src/database/models')

const routes = require("./src/routes/routes")
const ErrorHandler = require("./src/errors/ErrorHandler")

app.use(cors())
app.use(express.json())
app.use("/mailer-logo",express.static(path.resolve(__dirname, "src", "resources", "mailer")))
app.use(routes)
app.use(ErrorHandler)

module.exports = { app }