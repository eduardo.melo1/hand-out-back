const mongoose = require('mongoose');

const Categoria = new mongoose.Schema({
    tipo: {
        type:String,
        required:true,
        enum: ['In Natura','Minimamente Processado','Ultra Processado','Processado']
    }
})

module.exports = mongoose.model('Categoria', Categoria);