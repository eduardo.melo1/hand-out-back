const mongoose = require('mongoose');

const Favorite = new mongoose.Schema({
    id_user: {type:mongoose.Schema.Types.ObjectId, required:true, ref: 'User'},
    id_fav: {type:mongoose.Schema.Types.ObjectId, required:true, ref: 'User'},
    date: {type:Date}
})

module.exports = mongoose.model('Favorite', Favorite);