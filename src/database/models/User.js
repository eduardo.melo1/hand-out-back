const mongoose = require('mongoose');

const User = new mongoose.Schema({
    email: {type:String, required:true, trim:true},
    senha: {type:String, required:true},
    nome: {type:String, required:true, maxLength:100, trim:true},
    descricao: {type: String, required:false},
    isInstituicao: {type:Boolean, default:false},
    isEmailVerified: {type:Boolean, default:false},
    horarios: [new mongoose.Schema({
        date: {type:String},
        horario_inicio: {type:String},
        horario_fim: {type:String}
    })],
    contacts:[new mongoose.Schema({
        ddd: {type:String, maxLength:2},
        telefone: {type:String}
    })],
    addresses:[new mongoose.Schema({
        cep: {type:String, required:true},
        rua: {type:String, required:true, maxLength:200, trim:true},
        cidade: {type:String, required:true, maxLength:50, trim:true},
        numero: {type:Number, required:true},
        complemento: {type:String, required:false, maxLength:50, trim:true},
        lat: {type:String, required:true},
        long: {type:String, required:true}
    })],
})

module.exports = mongoose.model('User', User);