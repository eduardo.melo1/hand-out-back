const mongoose = require('mongoose');

const Necessidade = new mongoose.Schema({
    id_inst: {type:mongoose.Schema.Types.ObjectId, required:true, ref: 'User'},
    nome: {type:String, required:true, maxLength:50, trim:true},
    descricao: {type:String, required:false, maxLenght: 500},
    id_categoria: {type:mongoose.Schema.Types.ObjectId, required:true, ref: 'Categoria'},
    isActive: {type:Boolean, default:true},
})

module.exports = mongoose.model('Necessidade', Necessidade);