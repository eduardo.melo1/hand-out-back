const success = require("../helpers/SuccessResponseHelper")
const UserService = require("../services/UserService")
const TokenService = require("../services/TokenService")
const MailHelper = require("../helpers/MailHelper")

module.exports = {

  async teste(request, response) {
    const teste = "suceso"
    return response.json(success({ teste }, "Teste com sucesso"))    
  },

  async createUser(request, response) {
    const { nome, email, senha, isInstituicao } = request.body
    let userId = await UserService.createUser(
      nome,
      email,
      senha,
      isInstituicao,
      request.html,
      request.subject
    )
    return response.json(success({ userId }, "Email sent to new user"))
  },

  async forgotPassword(request, response) {
    const { email } = request.body
    await MailHelper.sendMail(request.html, email, request.subject)
    return response.json(success({}, "Email sent to reset password"))
  }, 

  async resetUserPassword(request, response) {
    const { email } = request.tokenData
    const { senha } = request.body
    await UserService.resetUserPassword(email, senha)
    await TokenService.deleteUserTokens(email)
    return response.json(success("Password reset"))
  },

  async updateUserPassword(request, response) {
    const { email, senha } = request.body
    console.log(email, senha)
    await UserService.resetUserPassword(email, senha)
    return response.json(success("Password changed"))
  },

  async donorActivation(request, response) {
    const { email } = request.tokenData    
    const token = request.params.token
    let donorMail = await UserService.activateUserEmail(email)
    await TokenService.deleteUserTokens(token, email)
    return response.json(success({ donorMail }, "Account activated. You may login"))
  },

  async institutionActivation(request, response) {
    const { email } = request.tokenData
    const token = request.params.token
    const { telefones, enderecos, horarios, descricao } = request.body 
    /* chamar o servico de cadastro de informações instituicao */
    await UserService.createInstitutionInfo(email, telefones, enderecos, horarios, descricao)
    await TokenService.deleteUserTokens(token, email)
    let institutionId = await UserService.activateUserEmail(email)
    return response.json(success({ institutionId }, "Account activated. You may login"))
  },

  async favoriteInstitution(request, response) {
    const { id, id_inst } = request.params
    let favorite = await UserService.favoriteInstitution(id_inst, id)
    return response.json(success({ favorite }, "Institution favourited"))
  },

  async showFavorites(request, response) {
    const { id } = request.params
    let favorites = await UserService.showFavorites(id)
    return response.json(success({ favorites }, "Favorite Instituions"))
  },

  async deleteFavorite(request, response) {
    const { id, id_inst } = request.params
    let favorite = await UserService.deleteFavorite(id_inst, id)
    return response.json(success({ favorite }, "Favorite Deleted"))
  },

  async showInfo(request, response) {
    const { userLoginId } = request.tokenData
    let userData = await UserService.getUser(userLoginId)
    return response.json(success({userData}, "Dados do Usuário"))
  },

  async showNecessities(request, response) {
    const { userLoginId } = request.tokenData
    let necessities = await UserService.getNecessities(userLoginId)
    return response.json(success({necessities}, "Necessidades da Instituição"))
  },

  async showInstitutions(request, response) {
    let institutions = await UserService.getInstitutions()
    return response.json(success({institutions}, "Instituições"))
  },

  async showInstitution(request, response) {
    const { id } = request.params
    let institution = await UserService.getInstitution(id)
    return response.json(success({institution}, "Instituição"))
  },

  async updateInstitution(request, response) {
    const { userLoginId } = request.tokenData
    const { telefones, enderecos, horarios } = request.body 
    let institution = await UserService.updateInstitution(userLoginId, telefones, enderecos, horarios)
    return response.json(success({institution}, "Instituição Modificada"))
  },

  async disableNecessity(request, response) {
    const { id, necessityId } = request.params
    await UserService.disableNecessity(id, necessityId)
    return response.json(success("Necessidade Desabilitada"))
  },

  async showCategories(request, response) {
    let categories = await UserService.getCategories()
    return response.json(success({categories},"Categorias de Necessidade"))
  },

  async enableNecessity(request, response) {
    const { id, necessityId } = request.params
    await UserService.enableNecessity(id, necessityId)
    return response.json(success("Necessidade Habilitada"))
  },

  async createNecessity(request, response) {
    const { userLoginId } = request.tokenData
    const { nome, descricao, id_categoria } = request.body
    let necessity = await UserService.createNecessity(userLoginId, nome, descricao, id_categoria)
    return response.json(success({necessity}, "Necessidade Criada"))
  }
}