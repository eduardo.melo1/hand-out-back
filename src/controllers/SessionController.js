require("dotenv").config()
const sessionService = require("../services/SessionService")
const success = require("../helpers/SuccessResponseHelper")

module.exports = {

  async createSession(request, response) {
    const { email, senha } = request.body
    const { token, isInstituicao } = await sessionService.createSession(email, senha)
    response.json(success({ token, isInstituicao }, "ok"))
  },

}