const express = require("express")
const router = express.Router()
const { validate } = require("../../middlewares/ValidationsMiddleware")
const UserController = require("../../controllers/UserController")
const AuthMiddleware = require("../../middlewares/AuthMiddleware")
const MailMiddleware = require("../../middlewares/MailMiddleware")

router.post(
  "/register",
  validate,
  MailMiddleware.userRegistration,
  UserController.createUser 
)

router.patch(
  "/register/verify/donor/:token",
  AuthMiddleware.verifyTokenInDB,
  UserController.donorActivation
)

router.post(
  "/recover",
  validate,
  MailMiddleware.forgotPassword,
  UserController.forgotPassword
)

router.patch(
  "/recover/verify/:token",
  AuthMiddleware.verifyTokenInDB,
  validate,
  UserController.resetUserPassword
)

router.patch(
  "/register/verify/institution/:token",
  AuthMiddleware.verifyTokenInDB,
  validate,
  UserController.institutionActivation
)

router.get(
  "/my-account",
  AuthMiddleware.verifyToken,
  UserController.showInfo
)

router.get(
  "/show-necessities",
  AuthMiddleware.verifyTokenLevel,
  UserController.showNecessities
)

router.get(
  "/show-institutions",
  AuthMiddleware.verifyToken,
  UserController.showInstitutions
)

router.get(
  "/show-institution/:id",
  AuthMiddleware.verifyToken,
  UserController.showInstitution
)

// :id é o id do usuario que ira favoritar a instituicao cujo id eh id_inst
router.post(
  "/favorite/:id/:id_inst",
  AuthMiddleware.verifyToken,
  UserController.favoriteInstitution
)

// :id eh o id do usuario que esta consultando seus favoritos
router.get(
  "/favorites/:id",
  AuthMiddleware.verifyToken,
  UserController.showFavorites
)

// :id é o id do usuario que ira deixar de favoritar a instituicao cujo id eh id_inst
router.delete(
  "/delete-favorite/:id/:id_inst",
  AuthMiddleware.verifyToken,
  UserController.deleteFavorite
)

router.patch(
  "/change-institution",
  validate,
  AuthMiddleware.verifyTokenLevel,
  UserController.updateInstitution
)

// email do usuario do token e senha do body
router.patch(
  "/change-password",
  validate,
  AuthMiddleware.verifyToken,
  UserController.updateUserPassword
)

router.get(
  "/show-categories",
  AuthMiddleware.verifyToken,
  UserController.showCategories
)

router.patch(
  "/show-necessities/:id/disable-necessity/:necessityId",
  AuthMiddleware.verifyTokenLevel,
  UserController.disableNecessity
)

router.patch(
  "/show-necessities/:id/enable-necessity/:necessityId",
  AuthMiddleware.verifyTokenLevel,
  UserController.enableNecessity
)

router.post(
  "/new-necessity",
  validate,
  AuthMiddleware.verifyTokenLevel,
  UserController.createNecessity
)

module.exports = router
