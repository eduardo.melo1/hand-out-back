const express = require("express")
const router = express.Router()
const { validate } = require("../../middlewares/ValidationsMiddleware")
const SessionController = require("../../controllers/SessionController")

router.post(
  "/login",
  validate,
  SessionController.createSession
)

module.exports = router