const express = require("express")
const router = express.Router()

const AuthRoutes = require("./AuthRoutes")
const UserRoutes = require("./UserRoutes") 

router.use(AuthRoutes)
router.use(UserRoutes)

module.exports = router
