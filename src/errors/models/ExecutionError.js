const { execution } = require("../../config/errorConfig")

class ExecutionError extends Error {
  constructor(errorType, errorKey) {
    super();
    this.code = execution[errorType][errorKey].code;
    this.message = execution[errorType][errorKey].message;
    this.details = execution[errorType][errorKey].details;
    this.type = errorType;
  }
}

module.exports = ExecutionError;
