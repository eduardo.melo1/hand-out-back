const { execution } = require("../../config/errorConfig")
function formatError(error, request, response, next) {
  const details = {}
  error.details.forEach((detail) => {
    details[detail.context.key] = detail.message
  })

  return response.status(execution.badRequest.status).json({
    code: execution.badRequest.format.code,
    message: execution.badRequest.format.message,
    details,
  })
}

module.exports = formatError