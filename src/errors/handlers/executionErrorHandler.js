const { execution } = require("../../config/errorConfig")

function executionErrorHandler(error, request, response) {

  return response.status(execution[error.type].status).json({
    code: error.code,
    message: error.message,
    details: error.details,
  })

}

module.exports = executionErrorHandler