const { internal } = require('../../config/errorConfig')


function internalErrorHandler(error, request, response) {
    return response.status(internal.status).json(internal.response)
}


module.exports = internalErrorHandler