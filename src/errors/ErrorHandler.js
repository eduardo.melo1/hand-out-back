const ExecutionError = require("./models/ExecutionError");
const executionErrorHandler = require("./handlers/executionErrorHandler");
const internalErrorHandler = require("./handlers/internalErrorHandler");
const formatErrorHandler = require("./handlers/formatErrorHandler");

function HandleErrors(error, request, response, next) {
  const { status, message, details } = error;
  console.log(error);
  if (error instanceof ExecutionError) return executionErrorHandler(error, request, response);
  if (error.isJoi) return formatErrorHandler(error, request, response, next);
  return internalErrorHandler(error, request, response);
}

module.exports = HandleErrors;
