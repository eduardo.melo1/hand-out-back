const mongoose = require('mongoose')
const Token = mongoose.model('Token')

module.exports = {
  async saveRegistrationToken(token, email) {
    const t = await Token
      .create({
        token,
        email
      })
    return t
  },

  async searchTokenInDb(token) {
    const t = await Token
      .findOne({token}).exec()
    return t;
  },

  async deleteTokens(email) {
    const t = await Token
      .deleteMany({ email: email })
    console.log(t);
    return t
  },

/*   async deleteAllTokensByLoginId(loginId) {
    await connection("tokens").where({ login_id: loginId }).del()
  }, */
};