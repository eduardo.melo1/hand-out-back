const mongoose = require('mongoose')
const User = mongoose.model('User')
const Necessidade = mongoose.model('Necessidade')
const Categoria = mongoose.model('Categoria')
const Favorite = mongoose.model('Favorite')

module.exports = {

  async createUser(body, isEmailVerified = false) {
    const user = await User
      .create({
        ...body,
        horarios: [],
        contacts: [],
        addresses: [],
        isEmailVerified
      })
    return user
  },

  async verifyUserEmail(email) {
    const userVerify = await User.updateOne({ email: email }, { isEmailVerified: true })
    return userVerify
  },

  async createInstitutionInfo(id, telefones, enderecos, horarios, descricao) {
    const inst = await User.findById(id)
    inst.descricao = descricao
    telefones.forEach(element => {
      inst.contacts.push(element)
    });
    enderecos.forEach(element => {
      inst.addresses.push(element)
    });
    horarios.forEach(element => {
      inst.horarios.push(element)
    });
    await inst.save()
    const tel = inst.contacts
    const end = inst.addresses
    const hr = inst.horarios
    const desc = inst.descricao
    return { tel, end, hr, desc }
  },

  async updateUserPassword(email, password) {
    const user = await User.updateOne({ email }, { senha: password })
    return user
  },

  async findUserByMail(email) {
    const user = User
      .findOne({
        email: email
      })
      .exec()
    return user
  },

  async getInstitutions() {

    const insts = await User.aggregate([
      {"$match": {
        "isInstituicao": {$eq: true}
      }},
      {"$lookup": {
        "from": "necessidades",
        "localField": "_id",
        "foreignField": "id_inst",
        "as": "necessidades"
      }},
      {"$lookup": {
        "from": "categorias",
        "localField": "necessidades.id_categoria",
        "foreignField": "_id",
        "as": "categorias"
      }},
      {"$unset": [
        "senha",
        "email",
        "isInstituicao",
        "isEmailVerified",
        "horarios",
        "descricao",
        "contacts",
        "necessidades"
      ]},
    ])

    return insts;
  },

  async getInstitution(id) {
    const inst = await User.find({_id: id, isInstituicao: true }).select('-senha').exec();
    const necess = await Necessidade.find({ id_inst: id }).populate("id_categoria")
    return {inst, necess};
  },

  //FIX ME
  async updateInstitution(id, telefones, enderecos, horarios) {
    const inst = await User.findOne({_id: id, isInstituicao: true })
    let update = []
    update = { $set: { 'contacts':telefones, 'addresses':enderecos, 'horarios':horarios }}
    const instUpdate = await User.findOneAndUpdate({_id: inst._id}, update, {new: true}).exec()

    /* inst.contacts.pull()
    inst.addresses.pull()
    inst.horarios.pull() */
    
    // inst.addresses = []
    // inst.horarios = []
    /* telefones.forEach(element => {
      inst.contacts.push(element)
    });
    enderecos.forEach(element => {
      inst.addresses.push(element)
    });
    horarios.forEach(element => {
      inst.horarios.push(element)
    });
    await inst.save()
    const tel = inst.contacts
    const end = inst.addresses
    const hr = inst.horarios */
    return instUpdate
  },

  async favoriteInstitution(institution, user) {
    const favorite = await Favorite.create({id_user: user._id, id_fav: institution.inst[0]._id, date: Date.now()})
    return favorite
  },

  async deleteFavorite(institution, user) {
    const favorite = await Favorite.deleteOne({id_fav: institution.inst[0]._id, id_user: user._id})
    return favorite
  },

  async showFavorites(user) {
    const favorites = await Favorite.find({id_user: user._id})
    return favorites
  },

  async findUserById(id) {
    const user = await User.findById(id).select('-senha')
    return user
  },

  async findNecessitiesById(id) {
    const necessities = await Necessidade.find({ id_inst: id }).populate("id_categoria")
    return necessities
  },

  async getCategories() {
    const categories = await Categoria.find()
    return categories
  },

  async disableNecessityById(userId, necessityId) {
    const necessity = await Necessidade.updateOne({ _id: necessityId, id_user: userId }, { isActive: false })
    return necessity
  },

  async enableNecessityById(userId, necessityId) {
    const necessity = await Necessidade.updateOne({ _id: necessityId, id_user: userId }, { isActive: true })
    return necessity
  },

  async createNecessity(body, isActive = true) {
    const necessity = await Necessidade.create({ ...body, isActive })
    return necessity
  }

}