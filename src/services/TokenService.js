const TokenRepository = require("../repositories/TokenRepository")
const ExecutionError = require("../errors/models/ExecutionError")

module.exports = {

  async isTokenInDB(token, email) {
    const found = await TokenRepository.searchTokenInDb(token)
    if (!found) throw new ExecutionError("auth", "tokenIsMissingFromDb")
  },

  async deleteUserTokens(email) {
    await TokenRepository.deleteTokens(email)
  },
  
  async saveToken(token, email) {
    await TokenRepository.saveRegistrationToken(token, email)
  },

  async saveTokenAndLoginId(token, loginId) {
    await TokenRepository.saveRegistrationTokenAndLoginId(token, loginId)
  },

}
