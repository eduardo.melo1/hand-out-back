const UserRepository = require("../repositories/UserRepository")
require("dotenv").config()
const { encrypt, compare } = require("../helpers/EncryptHelper")
const { sendMail } = require("../helpers/MailHelper")
const errorCodes = require("../config/errorCodes")
const ExecutionError = require("../errors/models/ExecutionError")
const mongoose = require('mongoose')

module.exports = {

  async createUser(nome, email, senha, isInstituicao, html, subject) {
    await sendMail(html, email, subject)
    const hash = await encrypt(senha)  
    let userId = await UserRepository.createUser({
      nome,
      email,
      senha:hash,
      isInstituicao      
    })
    console.log(userId);
    return userId[0]
  },

  async checkIfUserAlreadyExists(email, action) {
    const userAlreadyExists = await UserRepository.findUserByMail(email)

    if(userAlreadyExists!==null && action == "REGISTER")
      throw new ExecutionError(...errorCodes.userAlreadyExists.throwParams)
    /* if (!userAlreadyExists._id && action == "RECOVER")
      throw new ExecutionError(...errorCodes.userNotFound.throwParams) */
    
    /* if (userAlreadyExists && action == "REGISTER")
      throw new ExecutionError(...errorCodes.userAlreadyExists.throwParams)
    if (!userAlreadyExists && action == "RECOVER")
      throw new ExecutionError(...errorCodes.userNotFound.throwParams) */
    return userAlreadyExists
  },

  async activateUserEmail(email) {
    const emailVerified = await UserRepository.verifyUserEmail(email);
    if (!emailVerified)
      throw new ExecutionError(...errorCodes.userNotFound.throwParams);
    return email
  },

  async resetUserPassword(email, senha) {
    const hash = await encrypt(senha)
    let loginId = await UserRepository.updateUserPassword(email, hash)
    return loginId
  },

  async createInstitutionInfo(email, tel, ends, hrs, desc) {
    const user = await this.findUserByMail(email)
    const info = await UserRepository.createInstitutionInfo(user._id, tel, ends, hrs, desc)
    if (!info)
      throw new ExecutionError(...errorCodes.userInfoIsMissig.throwParams);
    return info
  },

  async findUserByMail(email) {
    const user = await UserRepository.findUserByMail(email)
    if(!user)
        throw new ExecutionError(...errorCodes.userNotFound.throwParams)
    return user
  },

  async isUserMailVerified(email) {
    const user = await UserRepository.findUserByMail(email)   
    if (!user)
      throw new ExecutionError(...errorCodes.userNotFound.throwParams)
    if (!user.isEmailVerified)
      throw new ExecutionError(...errorCodes.userUnverified.throwParams)
    return true
  },

  async getUser(id) {
    const user = await UserRepository.findUserById(id)
    if(!user || user.isEmailVerified == false)
      throw new ExecutionError(...errorCodes.userNotFound.throwParams)
    return user
  },

  async getNecessities(id) {
    const necessities = await UserRepository.findNecessitiesById(id)
    if(!necessities)
      throw new ExecutionError(...errorCodes.necessityNotFound.throwParams)
    return necessities
  },

  async getInstitutions() {
    const institutions = await UserRepository.getInstitutions()
    if(!institutions)
      throw new ExecutionError(...errorCodes.institutionsNotFound.throwParams)
    return institutions
  },

  async getInstitution(id) {
    const institution = await UserRepository.getInstitution(id)
    if(!institution)
      throw new ExecutionError(...errorCodes.institutionsNotFound.throwParams)
    return institution
  },

  async favoriteInstitution(id_inst, id) {
    const user = await UserRepository.findUserById(id)
    if(!user)
      throw new ExecutionError(...errorCodes.userNotFound.throwParams)
    const institution = await UserRepository.getInstitution(id_inst)
    if(!institution)
    throw new ExecutionError(...errorCodes.institutionsNotFound.throwParams)

    const favorite = await UserRepository.favoriteInstitution(institution, user)
    return favorite
  },

  async deleteFavorite(id_inst, id) {
    const user = await UserRepository.findUserById(id)
    if(!user)
      throw new ExecutionError(...errorCodes.userNotFound.throwParams)
    const institution = await UserRepository.getInstitution(id_inst)
    if(!institution)
    throw new ExecutionError(...errorCodes.institutionsNotFound.throwParams)

    const favorite = await UserRepository.deleteFavorite(institution, user)
    return favorite
  },

  async showFavorites(id) {
    const user = await UserRepository.findUserById(id)
    if(!user)
      throw new ExecutionError(...errorCodes.userNotFound.throwParams)
    const favorites = await UserRepository.showFavorites(user)
    return favorites
  },


  async updateInstitution(id, telefones, enderecos, horarios) {
    const info = await UserRepository.updateInstitution(id, telefones, enderecos, horarios)
    if(!info)
      throw new ExecutionError(...errorCodes.institutionsNotFound.throwParams)
    return info
  },

  async getCategories() {
    const categories = await UserRepository.getCategories()
    if(!categories)
      throw new ExecutionError(...errorCodes.categoriesNotFound.throwParams)
    return categories
  },

  async disableNecessity(userId, necessityId) {
    const necessity = await UserRepository.disableNecessityById(userId,necessityId)
    if(!necessity)
      throw new ExecutionError(...errorCodes.necessityNotFound.throwParams)
    return necessity
  },

  async enableNecessity(userId, necessityId) {
    const necessity = await UserRepository.enableNecessityById(userId,necessityId)
    if(!necessity)
      throw new ExecutionError(...errorCodes.necessityNotFound.throwParams)
    return necessity
  },

  async createNecessity(id_inst, nome, descricao, id_categoria) {
    id_categoria = mongoose.Types.ObjectId(id_categoria)
    const necessity = await UserRepository.createNecessity({id_inst, nome, descricao, id_categoria})
    if(!necessity)
      throw new ExecutionError(...errorCodes.necessityNotCreated.throwParams)
    return necessity
  }
}