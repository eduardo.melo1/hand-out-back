require("dotenv").config()
const encryptHelper = require("../helpers/EncryptHelper")
const tokenHelper = require("../helpers/TokenHelper")
const UserService = require("../services/UserService")
const expiresIn = process.env.AUTH_TOKEN_EXPIRATION


module.exports = {

  async createSession(email, senha) {
    const user = await UserService.findUserByMail(email)
    await UserService.isUserMailVerified(email)
    await encryptHelper.compare(senha, user.senha)
    const token = tokenHelper.signToken(
      { userLoginId:user._id, isInstituicao:user.isInstituicao, expiresIn },
      process.env.AUTH_TOKEN_SECRET,
      { expiresIn }
    )
    return { token, isInstituicao:user.isInstituicao }
  }

}