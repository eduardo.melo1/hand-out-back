require("dotenv").config()
const TokenHelper = require("../helpers/TokenHelper")
const TokenService = require("../services/TokenService")

module.exports = {

  verifyToken(request, response, next) {
    const token = TokenHelper.wasTokenSent(request.headers);
    const decoded = TokenHelper.isTokenValid(
      token,
      process.env.AUTH_TOKEN_SECRET
    )
    request.tokenData = decoded
    next()
  },

  verifyTokenLevel(request, response, next) {
    const token = TokenHelper.wasTokenSent(request.headers);
    const decoded = TokenHelper.isTokenValidLevel(
      token,
      process.env.AUTH_TOKEN_SECRET
    )
    request.tokenData = decoded
    next()
  },

  async verifyTokenInDB(request, response, next) {
    const token = TokenHelper.wasTokenSentInParams(request.params.token)
    const decode = TokenHelper.isTokenValid(
      token,
      process.env.MAILER_TOKEN_SECRET
    )
    await TokenService.isTokenInDB(token)
    request.tokenData = decode
    next()
  },

}