const MailHelper = require("../helpers/MailHelper")
const TokenHelper = require("../helpers/TokenHelper")
const TokenService = require("../services/TokenService")
const UserService = require("../services/UserService")

module.exports = {

  async userRegistration(request, response, next) {
    const { email, isInstituicao } = request.body
    await UserService.checkIfUserAlreadyExists(email, "REGISTER")
    const token = TokenHelper.signToken(
      { email },
      process.env.MAILER_TOKEN_SECRET,
      { expiresIn: "7 days" }
    )
    let link
    if(isInstituicao)
      link = MailHelper.prepareLink("/register/verify/institution/", token)
    else
      link = MailHelper.prepareLink("/register/verify/donor/", token)
    request.html = await MailHelper.prepareMailHtml(
      link,
      "/../resources/mailer/emailConfirmation.ejs"
    )
    request.subject = "Ativação de conta Hand Out"
    await TokenService.saveToken(token, email)
    next()
  },

  async forgotPassword(request, response, next) {
    const { email } = request.body
    await UserService.checkIfUserAlreadyExists(email, "RECOVER")
    await UserService.findUserByMail(email)
    const token = TokenHelper.signToken(
      { email },
      process.env.MAILER_TOKEN_SECRET,
      { expiresIn: "1 days" }
    )
    const link = MailHelper.prepareLink("/recover/verify/", token)
    request.html = await MailHelper.prepareMailHtml(
      link,
      "/../resources/mailer/emailPasswordRecovery.ejs"
    )
    request.subject = "Recuperação de senha Hand Out"
    await TokenService.saveToken(token, email)
    next()
  }

}