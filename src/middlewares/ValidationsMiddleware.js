const ValidationHandler = require("../validations/ValidationHandler")

module.exports = {

  async validate(request, response, next) {
    const { userId, validationParamsFromMiddlewares } = request
    let validationParams = { userId }
    if(validationParamsFromMiddlewares){
        validationParams = {
            ...validationParams,
            ...validationParamsFromMiddlewares,
        }
    }
    const { validationSchema, dataSource } = await ValidationHandler[request.route.path](validationParams)
    await validationSchema.validateAsync(request[dataSource], { allowUnknown: true, abortEarly: false })
    next() 
  }

}