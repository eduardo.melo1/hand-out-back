const joiExtensions = require("../../helpers/JoiExtensionHelper")
const Joi = require("joi").extend(joiExtensions)

module.exports = {

  "/new-necessity": async () => {
    return {
      validationSchema: Joi.object({
        nome: Joi.string().max(100).required(),
        descricao: Joi.string().required(),
        id_categoria: Joi.string().required()
      }),
      dataSource: "body",
    };
  },

  "/register": async () => {
    return {
      validationSchema: Joi.object({
        nome: Joi.string().max(100).required(),
        email: Joi.string().email().required(),
        senha: Joi.string().required().min(6).max(12),
        isInstituicao: Joi.boolean().required()
      }),
      dataSource: "body",
    };
  },

  "/register/verify/institution/:token": async () => {
    return {
      validationSchema: Joi.object({
        telefones: Joi.array()
        .items(
          Joi.object({
            ddd: Joi.string().max(2).required(),
            telefone: Joi.alternatives()
              .try(
                Joi.string().length(8).regex(/^[0-9]+$/),
                Joi.string().length(9).regex(/^[0-9]+$/),
              ).required(),            
          })
        )
        .unique((a, b) => a.telefone === b.telefone)
        .required()
        .min(1),
        enderecos: Joi.array()
        .items(
          Joi.object({
            cep: Joi.string().length(8).regex(/^[0-9]{8}$/).required(),
            rua: Joi.string(),
            cidade: Joi.string().required(),
            numero: Joi.number().required(),
            lat: Joi.string().required(),
            long: Joi.string().required()
          })
        )
        .unique((a, b) => a.cep === b.cep)
        .required()
        .min(1),
        horarios: Joi.array()
        .items(
          Joi.object({
            date: Joi.string().required(),
            horario_inicio: Joi.string().required(),
            horario_fim: Joi.string().required(),
          })
        )
        .required()
        .min(1)
        .unique((a, b) => a.date === b.date),
      }),
      dataSource: "body",
    };
  },

  "/recover": async () => {
    return {
      validationSchema: Joi.object({
        email: Joi.string().email().required(),
      }),
      dataSource: "body",
    };
  },

  "/recover/verify/:token": async () => {
    return {
      validationSchema: Joi.object({
        senha: Joi.string().required().min(6).max(12),
        confirmacaoSenha: Joi.string().required().valid(Joi.ref("senha")),
      }),
      dataSource: "body",
    }
  },

  "/change-institution": async () => {
    return {
      validationSchema: Joi.object({
        telefones: Joi.array()
        .items(
          Joi.object({
            ddd: Joi.string().max(2).required(),
            telefone: Joi.alternatives()
              .try(
                Joi.string().length(8).regex(/^[0-9]+$/),
                Joi.string().length(9).regex(/^[0-9]+$/),
              ).required(),            
          })
        )
        .unique((a, b) => a.telefone === b.telefone)
        .required()
        .min(1),
        enderecos: Joi.array()
        .items(
          Joi.object({
            cep: Joi.string().length(8).regex(/^[0-9]{8}$/).required(),
            rua: Joi.string(),
            cidade: Joi.string().required(),
            numero: Joi.number().required(),
            lat: Joi.string().required(),
            long: Joi.string().required()
          })
        )
        .unique((a, b) => a.cep === b.cep)
        .required()
        .min(1),
        horarios: Joi.array()
        .items(
          Joi.object({
            date: Joi.string().required(),
            horario_inicio: Joi.string().required(),
            horario_fim: Joi.string().required(),
          })
        )
        .required()
        .min(1)
        .unique((a, b) => a.date === b.date),
      }),
      dataSource: "body",
    };
  },

  "/change-password": async () => {
    return {
      validationSchema: Joi.object({
        email: Joi.string().email().required(),
        senha: Joi.string().required().min(6).max(12),
      }),
      dataSource: "body",
    }
  },

}