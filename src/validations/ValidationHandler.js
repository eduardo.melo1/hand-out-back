const SessionValidate = require("./SessionValidate")
const UserValidate = require("./UserValidate")

module.exports = {
    ...SessionValidate,
    ...UserValidate,
}