const joiExtensions = require("../../helpers/JoiExtensionHelper")
const Joi = require("joi").extend(joiExtensions)

module.exports = {

  "/login": async () => {
    return {
      validationSchema: Joi.object({
        email: Joi.string().email().required(),
        senha: Joi.string().required().min(6).max(12),
      }),
      dataSource: "body",
    }
  }

}