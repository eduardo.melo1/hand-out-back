module.exports = {
  
    internal: { code: 1, throwParams: [] },
    tokenIsMissing: { code: 2, throwParams: ["auth", "tokenIsMissing"] },
    invalidToken: { code: 3, throwParams: ["auth", "invalidToken"] },
    tokenIsMissingFromDb: {
      code: 4,
      throwParams: ["auth", "tokenIsMissingFromDb"],
    },
    userAlreadyExists: {
      code: 5,
      throwParams: ["unprocessable", "userAlreadyExists"],
    },  
    userNotFound: { code: 6, throwParams: ["unprocessable", "userNotFound"] },
    format: { code: 7, throwParams: ["badRequest", "format"] },
    invalidCredentials: {
      code: 8,
      throwParams: ["unauthorized", "invalidCredentials"],
    },
    userUnverified: { code: 9, throwParams: ["auth", "userUnverified"] },
    userInfoIsMissig: {
      code: 16,
      throwParams: ["unprocessable", "userInfoIsMissig"],
    },
    noPermissionToken: { 
      code: 29, 
      throwParams: ["auth", "noPermissionToken"] 
    },
    userAlreadyVerified: {
      code: 30,
      throwParams: ["unprocessable", "userAlreadyVerified"],
    },
    institutionsNotFound: {
      code: 31,
      throwParams: ["unprocessable", "institutionsNotFound"],
    },
    necessityNotCreated: {
      code: 32,
      throwParams: ["unprocessable", "necessityNotCreated"],
    },
    necessityNotFound: {
      code: 33,
      throwParams: ["unprocessable", "necessityNotFound"],
    },
    categoriesNotFound: {
      code: 34,
      throwParams: ["unprocessable", "categoriesNotFound"],
    },

  }