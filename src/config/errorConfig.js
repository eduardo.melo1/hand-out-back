const errorCodes = require("./errorCodes")

module.exports = {
  internal: {
    status: 500,
    response: {
      code: errorCodes.internal.code,
      message: "Internal Error. Try again later",
      details: {},
    },
  },
  execution: {
    auth: {
      status: 403,      
      userUnverified: {
        code: errorCodes.userUnverified.code,
        message: "User email unverified",
        details: {},
      },
      tokenIsMissing: {
        code: errorCodes.tokenIsMissing.code,
        message: "Authorization token is missing",
        details: {},
      },
      invalidToken: {
        code: errorCodes.invalidToken.code,
        message: "Authorization token is invalid",
        details: {},
      },
      noPermissionToken: {
        code: errorCodes.noPermissionToken.code,
        message: "This token is not allowed to access this service",
        details: {},
      },
      tokenIsMissingFromDb: {
        code: errorCodes.tokenIsMissingFromDb.code,
        message: "Authorization token is missing from databse",
        details: {},
      },
    },
    unauthorized: {
      status: 401,
      invalidCredentials: {
        code: errorCodes.invalidCredentials.code,
        message: "Invalid credentilas",
        details: {},
      },
    },
    unprocessable: {
      status: 422,
      userAlreadyExists: {
        code: errorCodes.userAlreadyExists.code,
        message: "Email already registered",
        details: {},
      },
      userAlreadyVerified: {
        code: errorCodes.userAlreadyVerified.code,
        message: "User Email already verified",
        details: {},
      },
      userNotFound: {
        code: errorCodes.userNotFound.code,
        message: "User email not found",
        details: {},
      },
      userInfoIsMissig: {
        code: errorCodes.userInfoIsMissig.code,
        message: "User id is missing",
        details: {},
      },
      institutionsNotFound: {
        code: errorCodes.institutionsNotFound.code,
        message: "Institutions not found",
        details: {},
      },
      necessityNotCreated: {
        code: errorCodes.necessityNotCreated.code,
        message: "Error create necessity",
        details: {},
      },
      necessityNotFound: {
        code: errorCodes.necessityNotFound.code,
        message: "Necessity not found",
        details: {},
      },
      categoriesNotFound: {
        code: errorCodes.categoriesNotFound.code,
        message: "Categories not found",
        details: {},
      },
    },
    badRequest: {
      status: 400,
      format: {
        code: errorCodes.format.code,
        message: "Bad request. Please check the provided information",
        details: {},
      },
    }
  }

} 