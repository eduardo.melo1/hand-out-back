const secure = process.env.MAILER_SECURE == 'true';
const rejectUnauthorized = process.env.MAILER_AUTH_TLS_REJECTUNAUTHORIZED == 'true';

module.exports = {
  mailers: {
    noReplyMailer: {
      host: process.env.MAILER_HOST,
      port: process.env.MAILER_PORT,
     secure,
     auth: {
       user: process.env.MAILER_AUTH_USER,
       pass: process.env.MAILER_AUTH_PSW,
     },
     tls: {
       rejectUnauthorized
     }
    },
  }
}