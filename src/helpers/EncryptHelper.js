const bcrypt = require("bcrypt");
const ExecutionError = require("../errors/models/ExecutionError");
const errorCodes = require("../config/errorCodes")

module.exports = {

  async encrypt(string, saltRounds=5) {
    const hash = await bcrypt.hash(string, saltRounds)
    return hash
  },

  async compare(password, hashedPassword) {
    const passwordMatch = await bcrypt.compare(password, hashedPassword)
    if (!passwordMatch)
      throw new ExecutionError(...errorCodes.invalidCredentials.throwParams)
     
  },

}