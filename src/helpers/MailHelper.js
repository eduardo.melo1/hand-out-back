require("dotenv").config()
const { mailers } = require("../config/mailConfig")
const nodemailer = require("nodemailer")
const ejs = require("ejs")
const path = require("path")

module.exports = {
  prepareLink(pathString, token) {
    return `${process.env.MAILER_EMAIL_BASE_URL}${pathString}${token}`
  },
  
  async prepareMailHtml(link, ejsPath) {
    const logoUrl = `${process.env.SERVER_BASE_URL}/mailer-logo/logo.png`
    const html = await ejs.renderFile(path.join(__dirname + ejsPath), {
      link,
      logoUrl,
    })
    return html
  },
  
  async sendMail(html, to, subject) {
    try {
      const transporter = nodemailer.createTransport(mailers.noReplyMailer)
      await transporter.sendMail({
        from: `NoReply <${mailers.noReplyMailer.auth.user}>`,
        to,
        subject,
        html,
      })
    } catch (error) {
      console.error(error)
    }
  },

}