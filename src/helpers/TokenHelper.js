require("dotenv").config()
const ExecutionError = require("../errors/models/ExecutionError")
const errorCodes = require("../config/errorCodes")
const jwt = require("jsonwebtoken")

module.exports = {
  
  wasTokenSent(headers) {
    const authHeader = headers["authorization"]
    const token = authHeader && authHeader.split(" ")[1]
    if (!token) throw new ExecutionError("auth", "tokenIsMissing")
    return token
  },
  
  wasTokenSentInParams(token) {
    if (!token) throw new ExecutionError("auth", "tokenIsMissing")
    return token
  },
  
  isTokenValid(token, secret) {
    const decoded = jwt.verify(token, secret, (err, decoded) => {
      if (err) throw new ExecutionError("auth", "invalidToken")
        delete decoded["iat"]
        delete decoded["exp"]
        return decoded
      });
    return decoded
  },

  isTokenValidLevel(token, secret) {
    const decoded = jwt.verify(token, secret, (err, decoded) => {
      if (err)
        throw new ExecutionError(...errorCodes.invalidToken.throwParams)
      if (!decoded.isInstituicao)
        throw new ExecutionError(...errorCodes.noPermissionToken.throwParams)
        delete decoded["iat"]
        delete decoded["exp"]
        return decoded
      })
    return decoded
  },

  signToken(payload, secret, options) {
    const token = jwt.sign(payload, secret, options)
    return token
  },

}