const { cpf, cnpj } = require("cpf-cnpj-validator");

module.exports = validator = (joi) => ({
  type: "user",
  base: joi.string(),
  messages: {
    "document.cpf": "CPF invalido",
    "document.cnpj": "CNPJ invalido",
    cep: "CEP invalido"
  },
  rules: {
    cpf: {
      validate(value, helpers, args, options) {
        if (!cpf.isValid(value)) 
          return helpers.error("document.cpf")
        
        return value
      },
    },
    cnpj: {
      validate(value, helpers, args, options) {
        if (!cnpj.isValid(value))
          return helpers.error("document.cnpj")
        
        return value
      },
    },
    cep: {
      validate(value, helpers, args, options) {
        if (!/^[0-9]{8}$/.test(value)) 
          return helpers.error("cep")
        return value
      },
    }
  },
});
